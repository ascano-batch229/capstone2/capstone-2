const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers.js");
const auth = require("../auth.js");

router.post("/registration", (req, res) => {
	userControllers.userRegistration(req.body)
	.then(fromController => res.send(fromController));
})

router.post("/login", (req, res) => {
	userControllers.userLogin(req.body)
	.then(fromController => res.send(fromController));
})


router.post("/newOrder", auth.verify, (req, res) =>{

	const data = {
		productId: req.body.productId,
		quantity: req.body.quantity, 
		email: auth.decode(req.headers.authorization).email,
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin 
	}

	userControllers.newOrder(data)
	.then(fromController => res.send(fromController))
    .catch(error => {
        console.log(error);
        res.status(500).send({error: "An error occurred"});
    });
});


router.get("/details", auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization);

	userControllers.userAccount({userId: req.body.id})
	.then(fromController => res.send(fromController));
})

router.get("/myOrders", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	userControllers.userOrders({userId: req.body.id})
	.then(fromController => res.send(fromController));

})

router.get("/orders", auth.verify, (req, res) => {

	const data = auth.decode(req.headers.authorization).isAdmin;

	userControllers.allOrders(data)
	.then(fromController => res.send(fromController));

})


router.put("/:userId/setAdmin", auth.verify, (req,res) => {

	const data = {
		reqParams: req.params,
		user: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	userControllers.userAdmin(data)
	.then(fromController => res.send(fromController));

})


router.get("/all", (req,res) => {

	userControllers.registeredUsers()
	.then(fromController => res.send(fromController));
})



module.exports = router;