const Users = require("../models/Users.js");
const Products = require("../models/Products.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

module.exports.userRegistration = (reqBody) => {

	let newUser = new Users({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		userName: reqBody.userName,		
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save()
	.then((user, error) => {
		if(error){
			return false;
		}else{
			return ("Registration Sucessfull!");
		}
	})

}

module.exports.userLogin = (reqBody) => {

	return Users.findOne({email: reqBody.email})
	.then(result => {

		if(result == null){
			return false;
		}else{

			const isPasswordMatch = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordMatch){
				return {access: auth.createAccessToken(result)}
			}else{
				return false;
			}
		}
	})
}

module.exports.newOrder = async (data) => {

	if(data.isAdmin == true){
		return ("Unable to proceed!")
		}

		  // Find the product using the provided productId
			  const product = await Products.findById(data.productId);

			  // Check if the product is found
			  if (!product) {
			    return { error: "Product not found" };
			  }

			  // Get the product name and price
			  const { productName, price } = product;

			  // Find the user using the provided userId
			  const user = await Users.findById(data.userId);

			  // Check if the user is found
			  if (!user) {
			    return { error: "User not found" };
			  }

			  user.newOrders.push({
			    products: [{ productName: productName, price: price, quantity: data.quantity }],
			    totalAmount: data.quantity * price,
			    purchasedOn: new Date(),			    
			    
			  });

			  if(user.newOrders.length > 0){
				user.total = user.newOrders.reduce((total, order) => total + order.totalAmount, 0);
				}


			  product.orders.push({
			  	userId: data.userId,
			  	price: price,
			  	quantity: data.quantity,
			  	email: data.email
			  })


			  // Save the user to the database
			  await user.save();
			  await product.save();

			  // Send a success response to the client
			  return { message: "Order placed successfully" };

}


module.exports.userAccount = (data) => {

    return Users.findById(data.userId).then(result =>{

        console.log(result)

        result.password = "";

        return result;

    }).catch(error => {

        console.log(error);

    });
}

module.exports.userAdmin = (data) => {

	if(data.isAdmin == true){

		let setAsAdmin = {
			isAdmin: data.user.isAdmin
		}

		return Users.findByIdAndUpdate(data.reqParams.userId, setAsAdmin)
		.then((user,error) => {
			if(error){
				return false;
			}else 
			return ("User is now set as Admin");
		})
	}else{
		return Promise.resolve("Not authorized to make changes");
	}
}


module.exports.userOrders = (data) => {
    return Users.findById(data.userId)    
    .then(result => {

        let user = {
            firstName: result.firstName,
            lastName: result.lastName,
            newOrders: result.newOrders,
            total: result.total
        }


        return user;

    }).catch(error => {
        console.log(error);
    });
}



module.exports.registeredUsers = () => {

	return Users.find({})
	.then(result => {
		return result;
	})
}

module.exports.allOrders = (data) => {

	if(data){

		return Users.find({}, 
			{firstName: 1, lastName: 1, newOrders:1, total: 1})

		.then(result => {

			return result;
		})

	}else{
		return ("Not authorized!")
	}
}